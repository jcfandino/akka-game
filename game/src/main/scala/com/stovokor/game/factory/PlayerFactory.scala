package com.stovokor.game.factory

import com.jme3.asset.AssetManager
import com.jme3.math.ColorRGBA
import com.jme3.scene.Geometry
import com.jme3.scene.shape.Box
import com.jme3.bullet.objects.PhysicsRigidBody
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape

object PlayerFactory {

  val playerWidth = 0.5f
  val playerLength = 0.25f
  val playerHeight = 2f

  def create(assetManager: AssetManager) = {
    val box = new Geometry("player", new Box(playerWidth / 2f, playerLength / 2f, playerHeight / 2f))
    box.setMaterial(MaterialFactory.create(assetManager, ColorRGBA.Pink))
    //    box.setQueueBucket(Bucket.Transparent);
    //    val body = new RigidBodyControl(1f)
    //    val bcc = new BetterCharacterControl(playerWidth, playerHeight, 100f)
    //    bcc.setGravity(new Vector3f(0, 0, -10f))

    //    box.addControl(bcc)
    //    box.addControl(PlayerControl(inputManager))

    //    box.move(2, 2, 1)
    box
    //    var carNode = new Node("car")
    //    carNode.attachChild(box)
    //    
  }

  def createPhysics() = {
    //    val shape = new CapsuleCollisionShape(.5f, 2f, 1)
    val shape = new CapsuleCollisionShape(playerWidth / 2f, playerHeight, 1)
    val body = new PhysicsRigidBody(shape, 100f)
    //    body.setKinematic(true)
    body.setAngularFactor(0)
    body
  }

}