package com.stovokor.game.factory

import com.jme3.asset.AssetManager
import com.jme3.bullet.objects.PhysicsRigidBody
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape
import com.jme3.math.Quaternion
import com.jme3.math.FastMath
import com.jme3.scene.Node
import com.jme3.scene.Geometry
import com.jme3.scene.shape.Box
import com.jme3.math.ColorRGBA
import com.jme3.bullet.collision.shapes.BoxCollisionShape
import com.jme3.math.Vector3f
import com.jme3.bullet.collision.shapes.CompoundCollisionShape

object AmmoPickupFactory {

  val size = .2f

  def create(assetManager: AssetManager) = {
    val box = new Geometry("ammo", new Box(size / 2f, size / 2f, size / 2f))
    val material = MaterialFactory.create(assetManager, ColorRGBA.Orange)
    box.setMaterial(material)
    box.setLocalTranslation(0, size, 0)
    val node = new Node("ammo")
    node.attachChild(box)
    node
  }

  def createPhysics() = {
    val box = new BoxCollisionShape(new Vector3f(size / 2f, size / 2f, size / 2f))
    val shape = new CompoundCollisionShape()
    shape.addChildShape(box, new Vector3f(0, size, 0))
    val body = new PhysicsRigidBody(shape)
    body.setKinematic(true)
    body
  }
}