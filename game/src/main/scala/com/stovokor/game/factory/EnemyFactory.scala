package com.stovokor.game.factory

import com.jme3.asset.AssetManager
import com.jme3.bullet.objects.PhysicsRigidBody
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape
import com.jme3.math.Quaternion
import com.jme3.math.FastMath
import com.jme3.scene.Node

object EnemyFactory {

  val enemyWidth = 1f
  val enemyLength = .6f
  val enemyHeight = 2f

  def create(assetManager: AssetManager) = {
    val model = assetManager.loadModel("Models/Ninja/Ninja.mesh.xml")
    val material = MaterialFactory.create(assetManager, "Models/Ninja/Ninja.jpg")
    model.setMaterial(material)
    model.setLocalScale(0.01f)
    model.setLocalRotation(new Quaternion().fromAngles(-FastMath.HALF_PI, 0, FastMath.PI))
    model.setLocalTranslation(0, 0, enemyHeight / 2f)

    val node = new Node("enemy")
    node.attachChild(model)
    node
  }

  def createPhysics() = {
    val shape = new CapsuleCollisionShape(enemyWidth / 2f, enemyHeight - enemyWidth)
    val body = new PhysicsRigidBody(shape)
    body.setAngularFactor(0)
    body
  }
}