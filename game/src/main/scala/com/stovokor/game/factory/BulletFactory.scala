package com.stovokor.game.factory

import com.jme3.asset.AssetManager
import com.jme3.bullet.collision.shapes.SphereCollisionShape
import com.jme3.bullet.objects.PhysicsRigidBody
import com.jme3.math.ColorRGBA
import com.jme3.scene.Geometry
import com.jme3.scene.shape.Sphere

object BulletFactory {

  val bulletWidth = .25f

  def create(assetManager: AssetManager) = {
    val ball = new Geometry("bullet", new Sphere(6, 6, bulletWidth))
    ball.setMaterial(MaterialFactory.create(assetManager, ColorRGBA.Yellow))
    //    ball.addControl(new RigidBodyControl(new SphereCollisionShape(bulletWidth), 1f))
    ball
  }

  def createPhysics() = {
    val shape = new SphereCollisionShape(bulletWidth)
    val body = new PhysicsRigidBody(shape, 1f)
    body.setKinematic(true)
    body
  }
}