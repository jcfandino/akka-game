package com.stovokor.game

import com.stovokor.game.state.InitializeGameState

import com.jme3.app.SimpleApplication
import com.jme3.bullet.BulletAppState
import com.jme3.bullet.BulletAppState.ThreadingType
import com.jme3.system.AppSettings
import com.stovokor.state.ActorSystemState
import com.stovokor.game.state.FPSActorCameraState
import com.stovokor.game.state.PlayerMovementState
import com.stovokor.game.state.HudState
import com.jme3.math.Vector3f

object AkkaGame extends SimpleApplication {

  def main(args: Array[String]) {
    val sets = new AppSettings(true)
    sets.setGammaCorrection(true)
    sets.setWidth(1024)
    sets.setHeight(768)
    setSettings(sets)
    setDisplayFps(true)
    setDisplayStatView(false)

    AkkaGame.start
  }

  def simpleInitApp() = {
    getFlyByCamera.setEnabled(false)
    getInputManager.setCursorVisible(false)

    val bulletAppState = new BulletAppState
    //    bulletAppState.setDebugEnabled(true)
    bulletAppState.setThreadingType(ThreadingType.PARALLEL)
    //    bulletAppState.getPhysicsSpace.setGravity(new Vector3f(0f, -10f, 0f))
    //    stateManager.attach(new DetailedProfilerState())
    stateManager.attach(bulletAppState)
    stateManager.attach(new ActorSystemState())
    stateManager.attach(new PlayerMovementState())
    stateManager.attach(new FPSActorCameraState())
    //    stateManager.attach(new TopDownActorCameraState())
    stateManager.attach(new InitializeGameState)
    stateManager.attach(new HudState)
  }

}