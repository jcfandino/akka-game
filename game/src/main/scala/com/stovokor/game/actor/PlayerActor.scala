package com.stovokor.game.actor

import com.jme3.math.Quaternion
import com.jme3.math.Vector3f
import com.stovokor.actor.AskMovement
import com.stovokor.actor.AskPosition
import com.stovokor.actor.PositionResponse
import com.stovokor.actor.TellPosition
import com.stovokor.game.actor.domain.EntityMovement

import akka.actor.Actor
import akka.actor.Props
import akka.pattern._
import akka.util.Timeout
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import akka.actor.ActorRef

class PlayerActor(var initPos: Vector3f, rot: Quaternion, var health: Int = 100, ammo: Int) extends Actor {

  implicit val timeout = Timeout(1.seconds)

  val pos = initPos.clone()
  var weapon = context.actorOf(Props(new WeaponActor(ammo, 100)), "weapon")
  val movement = new EntityMovement(10f)

  override def preStart() {
  }

  def receive = {
    case PlayerMove(delta)         => movement.updateMovement(delta)
    case PlayerRotate(rotH, rotV)  => movement.updateRotation(rotH, rotV)
    case TellPosition(newPos)      => pos.set(newPos)
    case ResetPosition(pos, rot)   => movement.reset(pos, rot)
    case AskPosition               => sender ! PositionResponse(pos, movement.rotation)
    case AskMovement               => sender ! movement.respondMovement
    case Shoot                     => weapon ! ShootBullet(pos, movement.direction)
    case Hit(damage)               => health = (health - damage.intValue).max(0)
    case OfferPickupAmmo(amount)   => pickupItem(sender, weapon, OfferPickupAmmo(amount))
    case OfferPickupHealth(amount) => pickupHealth(sender, amount)
    case AskPlayerState            => respondState(sender)
  }

  def respondState(to: ActorRef) {
    (weapon ? AskAmmoAmount).foreach({
      // TODO fix this cast
      case x => to ! PlayerStateResponse(x.asInstanceOf[Int], health)
    })
  }

  def pickupItem(from: ActorRef, to: ActorRef, offer: Any) {
    (to ? offer).foreach({
      case true  => from ! OfferPickupAccepted
      case false =>
    })
  }

  def pickupHealth(from: ActorRef, amount: Int) {
    if (health < 100) {
      health = (health + amount).min(100)
      from ! OfferPickupAccepted
    }
  }
}