package com.stovokor.game.actor

import akka.actor.Props
import com.jme3.math.Vector3f
import com.jme3.math.Quaternion
import akka.actor.ActorRef
import com.jme3.bullet.collision.PhysicsCollisionEvent

case class PlayerMove(delta: Vector3f)
case class PlayerRotate(rotH: Float, rotV: Float)
case object Shoot
case class ShootBullet(pos: Vector3f, dir: Vector3f)
case class Hit(damage: Float)
case class OfferPickupAmmo(amount: Int)
case class OfferPickupHealth(amount: Int)
case object OfferPickupAccepted
case object AskPlayerState
case object AskAmmoAmount
case class PlayerStateResponse(ammo: Int, health: Int)
case object AskLevelState
case class LevelStateResponse(level: Int, complete: Boolean)
case object FinishLevel
case class ResetPosition(pos: Vector3f, rot: Quaternion)