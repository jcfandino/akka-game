package com.stovokor.game.actor

import akka.actor.Actor
import com.stovokor.actor.CollideWith

class HealthPickupActor(val amount: Int) extends Actor {

  def receive = {
    case CollideWith(other, event) => other ! OfferPickupHealth(amount)
    case OfferPickupAccepted       => context.stop(self)
    case _                         =>
  }
}