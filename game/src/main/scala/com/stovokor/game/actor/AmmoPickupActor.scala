package com.stovokor.game.actor

import akka.actor.Actor
import com.stovokor.actor.CollideWith

class AmmoPickupActor(val amount: Int) extends Actor {

  def receive = {
    case CollideWith(other, event) => other ! OfferPickupAmmo(amount)
    case OfferPickupAccepted       => context.stop(self)
    case _                         =>
  }
}