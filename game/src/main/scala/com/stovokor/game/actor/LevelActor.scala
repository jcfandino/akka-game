package com.stovokor.game.actor

import akka.actor.Actor
import com.stovokor.actor.CollideWith
import com.stovokor.actor.Tick

class LevelActor(val level: Int) extends Actor {

  var complete = false

  def receive = {
    case AskLevelState => sender ! LevelStateResponse(level, complete)
    case FinishLevel   => complete = true
  }
}