package com.stovokor.game.actor.domain

import scala.util.Random
import com.jme3.math.FastMath
import com.jme3.math.Vector3f
import com.stovokor.game.actor.ShootBullet
import akka.actor.ActorContext
import akka.actor.actorRef2Scala

class SampleEnemyBehavior extends EnemyBehavior {

  var turn = 1

  def update(context: ActorContext, state: EnemyState, tpf: Double) {
    state.movement.updateMovement(Vector3f.UNIT_X)
    val rotdelta = if (turn == 0) .01f else if (turn == 1) 0f else -.01f
    state.movement.updateRotation(state.movement.rotationH + rotdelta, FastMath.HALF_PI)
    if (Random.nextFloat() < 0.002f) {
      turn = Random.nextInt(3)
    }
    if (Random.nextFloat() < 0.01f) {
      context.child("weapon").foreach(weapon => weapon ! ShootBullet(state.pos, state.movement.direction))
    }
  }

}