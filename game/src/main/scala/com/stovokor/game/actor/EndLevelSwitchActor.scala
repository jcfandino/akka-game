package com.stovokor.game.actor

import akka.actor.Actor
import com.stovokor.actor.CollideWith

class EndLevelSwitchActor extends Actor {

  def receive = {
    case CollideWith(other, event) => {
      if (other.path.name == "player") {
        context.actorSelection("/user/level-*") ! FinishLevel
      }
    }
    case _ =>
  }
}