package com.stovokor.game.actor

import java.util.UUID

import com.stovokor.actor.PhysicsActorSpawnRequest
import com.stovokor.actor.SpatialActorSpawnRequest
import com.stovokor.game.factory.BulletFactory

import akka.actor.Actor
import akka.actor.ActorSelection.toScala
import akka.actor.Props

class WeaponActor(val initBullets: Int, val maxBullets: Int) extends Actor {

  var bullets = initBullets

  def receive = {
    case AskAmmoAmount => sender ! bullets
    case ShootBullet(pos, dir) => {
      if (bullets > 0) {
        bullets -= 1
      }
      // if bullets -1 then they're infinite
      if (bullets != 0) {
        val bulPos = pos.add(dir.mult(2f))
        val bullet = context.system.actorOf(
          Props(new BulletActor(bulPos, dir)), "bullet-" + UUID.randomUUID())

        val message1 = SpatialActorSpawnRequest(bulPos, bullet, BulletFactory.create)
        context.actorSelection("/user/spatialSpawner") ! message1

        val message2 = PhysicsActorSpawnRequest(bulPos, bullet, BulletFactory.createPhysics)
        context.actorSelection("/user/physicsSpawner") ! message2
      }
    }
    case OfferPickupAmmo(amount) => {
      sender ! (bullets < maxBullets)
      bullets = (bullets + amount).min(maxBullets)
    }
  }
}