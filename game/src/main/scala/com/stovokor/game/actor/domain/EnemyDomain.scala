package com.stovokor.game.actor.domain

import com.jme3.math.FastMath
import com.jme3.math.Vector3f
import scala.util.Random
import akka.actor.ActorContext

class EnemyState(
    var pos: Vector3f = new Vector3f,
    var health: Float = 50f,
    val movement: EntityMovement = new EntityMovement(2f)) {

  def setPosition(p: Vector3f) = pos.set(p)

}

trait EnemyBehavior {
  def update(context: ActorContext, state: EnemyState, tpf: Double)
}

