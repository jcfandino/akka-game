package com.stovokor.game.actor.domain

import com.jme3.math.FastMath
import com.stovokor.actor.MovementResponse
import com.jme3.math.Quaternion
import com.jme3.math.Vector3f
import com.stovokor.actor.RelativeMovement
import com.stovokor.actor.Movement
import com.stovokor.actor.AbsoluteMovement

/**
 * Holds the state of the desired movement of an entity (player or enemy actor).
 */
class EntityMovement(val movFactor: Float) {

  val nullMovement = RelativeMovement(Vector3f.ZERO, new Quaternion)

  // how much movement in the space
  var nextMove: Movement = nullMovement

  // horizontal rotation (look left/right)
  var rotationH = 0f // 0 ~ 2pi

  // vertical rotation (look up/down)
  var rotationV = FastMath.HALF_PI // 0 ~ pi

  def updateMovement(delta: Vector3f) {
    move(delta)
  }

  def updateRotation(rotX: Float, rotV: Float) {
    rotationH = rotX
    rotationV = rotV
  }

  def move(delta: Vector3f) {
    if (delta.x != 0f || delta.z != 0f) {
      val fx = FastMath.sin(rotationH)
      val fz = FastMath.cos(rotationH)
      val forward = new Vector3f(fx, 0f, fz).multLocal(delta.x)

      val sx = FastMath.sin(rotationH + FastMath.HALF_PI)
      val sz = FastMath.cos(rotationH + FastMath.HALF_PI)
      val side = new Vector3f(sx, 0f, sz).multLocal(delta.z)

      val dir = forward.addLocal(side).normalizeLocal.multLocal(movFactor)
      nextMove = RelativeMovement(dir, rotation)
    } else {
      nextMove = RelativeMovement(Vector3f.ZERO, rotation)
    }
  }

  def reset(pos: Vector3f, rot: Quaternion) {
    nextMove = AbsoluteMovement(pos, rot)
  }

  def respondMovement = MovementResponse(nextMove)
  def rotation = new Quaternion().fromAngles(rotationV, rotationH, 0f)
  def direction = rotation.mult(Vector3f.UNIT_Y) // quat x vec => rotates the vector
}