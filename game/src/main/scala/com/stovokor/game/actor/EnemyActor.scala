package com.stovokor.game.actor

import com.stovokor.actor.AskMovement
import com.stovokor.actor.AskPosition
import com.stovokor.actor.PositionResponse
import com.stovokor.actor.TellPosition
import com.stovokor.actor.Tick
import com.stovokor.game.actor.domain.EnemyBehavior
import com.stovokor.game.actor.domain.EnemyState

import akka.actor.Actor
import akka.actor.Props
import akka.actor.actorRef2Scala

class EnemyActor(val state: EnemyState, behavior: EnemyBehavior) extends Actor {

  var weapon = context.actorOf(Props(new WeaponActor(-1, -1)), "weapon")

  def receive = {
    case AskPosition          => sender ! PositionResponse(state.pos, state.movement.rotation)
    case TellPosition(newPos) => state.setPosition(newPos)
    case Tick(t)              => behavior.update(context, state, t)
    case Hit(damage) => {
      state.health -= damage
      if (state.health <= 0f) {
        println(s"Enemy: I'm dead!")
        context.stop(self)
      }
    }
    case AskMovement => sender ! state.movement.respondMovement
  }

}
