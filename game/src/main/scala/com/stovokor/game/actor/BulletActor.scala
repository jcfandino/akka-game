package com.stovokor.game.actor

import akka.actor.Actor
import com.jme3.math.Vector3f
import akka.actor.actorRef2Scala
import com.stovokor.actor.AskPosition
import com.stovokor.actor.CollideWith
import com.stovokor.actor.Tick
import com.stovokor.actor.PositionResponse

/**
 * Actor for the game bullets (not to be confused with the physics engine).
 */
class BulletActor(var loc: Vector3f, var dir: Vector3f) extends Actor {
  var ttl: Double = 10
  val speed = 30f

  def receive = {
    case AskPosition => sender ! PositionResponse(loc)
    case Tick(t) => {
      loc.addLocal(dir.mult(speed * t.toFloat))
      ttl -= t
      if (ttl <= 0) context.stop(self)
    }
    case CollideWith(other, event) => {
      other ! Hit(10)
      context.stop(self)
    }
  }
}