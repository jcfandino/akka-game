package com.stovokor.game.state

import com.jme3.app.state.AppStateManager
import com.jme3.app.Application
import com.stovokor.actor.AskPosition
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import akka.pattern._
import akka.util.Timeout
import com.jme3.math.Vector3f
import com.stovokor.actor.PositionResponse
import java.util.concurrent.Callable
import com.stovokor.state.ActorSystemState
import com.stovokor.state.BaseState

class TopDownActorCameraState extends BaseState {

  override def initialize(stateManager: AppStateManager, simpleApp: Application) {
    super.initialize(stateManager, simpleApp)
  }

  def system = stateManager.getState(classOf[ActorSystemState]).system

  override def update(tpf: Float) {
    implicit val timeout = Timeout(1.seconds)
    val answer = system.actorSelection("/user/player") ? AskPosition
    answer.foreach(pos => {
      val p = pos.asInstanceOf[PositionResponse]
      app.enqueue(new Callable[Unit]() {
        def call = {
          val ppos = new Vector3f(p.pos.getX, 0f, p.pos.getZ)
          cam.setLocation(ppos.add(1f, 20f, -2f))
          cam.lookAt(ppos, Vector3f.UNIT_X)
          cam.update
        }
      })
    })
  }
}
