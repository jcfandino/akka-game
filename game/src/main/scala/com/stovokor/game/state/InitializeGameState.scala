package com.stovokor.game.state

import com.jme3.app.Application
import com.jme3.app.SimpleApplication
import com.jme3.app.state.AbstractAppState
import com.jme3.app.state.AppStateManager
import com.jme3.bullet.BulletAppState
import com.jme3.bullet.control.RigidBodyControl
import com.jme3.math.FastMath
import com.jme3.math.Quaternion
import com.jme3.math.Vector3f
import com.stovokor.game.actor.EnemyActor
import com.stovokor.actor.PhysicsActorSpawnRequest
import com.stovokor.game.actor.PlayerActor
import com.stovokor.actor.SpatialActorSpawnRequest
import akka.actor.Props
import akka.actor.ActorSelection.toScala
import com.jme3.bullet.BulletAppState
import com.jme3.bullet.control.RigidBodyControl
import com.stovokor.state.ActorSystemState
import com.stovokor.game.factory.PlayerFactory
import com.stovokor.game.factory.EnemyFactory
import com.stovokor.game.factory.TestMapFactory
import com.stovokor.game.actor.domain.EnemyState
import com.stovokor.game.actor.domain.SampleEnemyBehavior
import com.stovokor.game.actor.domain.EnemyState
import com.stovokor.game.actor.AmmoPickupActor
import java.util.UUID
import com.stovokor.game.factory.AmmoPickupFactory
import com.stovokor.game.actor.HealthPickupActor
import com.stovokor.game.factory.HealthPickupFactory
import com.stovokor.state.BaseState
import akka.dispatch.sysmsg.Terminate
import akka.actor.PoisonPill
import akka.pattern._
import akka.util.Timeout
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success
import com.stovokor.actor.TellPosition
import com.stovokor.game.actor.PlayerRotate
import com.stovokor.game.actor.AskPlayerState
import com.stovokor.game.actor.PlayerStateResponse
import com.stovokor.game.actor.LevelActor
import com.stovokor.game.actor.AskLevelState
import com.stovokor.game.actor.LevelStateResponse
import com.stovokor.game.actor.EndLevelSwitchActor
import com.stovokor.game.factory.EndLevelSwitchFactory
import com.stovokor.game.actor.ResetPosition

// only does initialization
class InitializeGameState(val level: Int = 1) extends BaseState with ThrottledUpdate {

  implicit val timeout = Timeout(1.seconds)
  val maxAge = 1f / 60f // seconds

  val mapWidth = 100
  val mapHeight = 100

  override def initialize(stateManager: AppStateManager, simpleApp: Application) {
    super.initialize(stateManager, simpleApp)
    println(s"Initializing level $level")

    app.getFlyByCamera.setEnabled(false)
    app.getInputManager.setCursorVisible(false)

    val space = stateManager.getState(classOf[BulletAppState]).getPhysicsSpace
    space.setGravity(new Vector3f(0, -10, 0))

    // generated map
    val map = TestMapFactory.create(assetManager, mapWidth, mapHeight, level)
    val mapBody = TestMapFactory.createPhysics(map)
    // rotate because it's in other coordinates
    mapBody.setPhysicsRotation(new Quaternion().fromAngles(-FastMath.HALF_PI, -FastMath.HALF_PI, 0))
    map.setLocalRotation(new Quaternion().fromAngles(-FastMath.HALF_PI, -FastMath.HALF_PI, 0))
    // is an actor
    val levelActor = actorSystem.actorOf(Props(new LevelActor(level)), "level-" + level)
    spatialSpawner ! SpatialActorSpawnRequest(Vector3f.ZERO, levelActor, (_) => map)
    physicsSpawner ! PhysicsActorSpawnRequest(Vector3f.ZERO, levelActor, () => mapBody)

    // player
    val playerPos = new Vector3f(4, 2, 3)
    def spawnPlayer(ammo: Int, health: Int) {
      val player = actorSystem.actorOf(Props(new PlayerActor(playerPos, new Quaternion(), health, ammo)), "player")
      spatialSpawner ! SpatialActorSpawnRequest(playerPos, player, PlayerFactory.create)
      physicsSpawner ! PhysicsActorSpawnRequest(playerPos, player, PlayerFactory.createPhysics)
    }
    // try to find the player or create a new one
    actorSystem.actorSelection("/user/player").resolveOne().onComplete {
      case Success(player) => {
        player ! ResetPosition(playerPos, new Quaternion())
        val controller = stateManager.getState(classOf[PlayerMovementState])
        if (controller != null) {
          controller.reset(playerPos)
        }
      }
      case Failure(ex) => {
        println(s"creating new player")
        spawnPlayer(50, 100)
      }
    }

    // enemies
    val enemyPositions = List(
      new Vector3f(23, 1, 3), new Vector3f(43, 1, 3), new Vector3f(63, 1, 3),
      new Vector3f(43, 1, 23), new Vector3f(63, 1, 23), new Vector3f(83, 1, 23),
      new Vector3f(63, 1, 43), new Vector3f(83, 1, 43), new Vector3f(103, 1, 43))

    enemyPositions.zipWithIndex.foreach({
      case (enemyPos, idx) => {
        val state = new EnemyState(enemyPos)
        val enemy = actorSystem.actorOf(Props(new EnemyActor(state, new SampleEnemyBehavior)), "enemy-" + idx)
        spatialSpawner ! SpatialActorSpawnRequest(enemyPos, enemy, EnemyFactory.create)
        physicsSpawner ! PhysicsActorSpawnRequest(enemyPos, enemy, EnemyFactory.createPhysics)
      }
    })

    // items
    val ammoPositions = List(
      new Vector3f(16, 0, 3))
    ammoPositions.foreach(pos => {
      val pickup = actorSystem.actorOf(Props(new AmmoPickupActor(10)), "ammo-" + UUID.randomUUID)
      spatialSpawner ! SpatialActorSpawnRequest(pos, pickup, AmmoPickupFactory.create)
      physicsSpawner ! PhysicsActorSpawnRequest(pos, pickup, AmmoPickupFactory.createPhysics)
    })
    val healthPositions = List(
      new Vector3f(36, 0, 3))
    healthPositions.foreach(pos => {
      val pickup = actorSystem.actorOf(Props(new HealthPickupActor(50)), "health-" + UUID.randomUUID)
      spatialSpawner ! SpatialActorSpawnRequest(pos, pickup, HealthPickupFactory.create)
      physicsSpawner ! PhysicsActorSpawnRequest(pos, pickup, HealthPickupFactory.createPhysics)
    })

    // end level switch
    val switchPos = new Vector3f(3, 0, 16)
    val switch = actorSystem.actorOf(Props(new EndLevelSwitchActor()), "end-level-switch-" + level)
    spatialSpawner ! SpatialActorSpawnRequest(switchPos, switch, EndLevelSwitchFactory.create)
    physicsSpawner ! PhysicsActorSpawnRequest(switchPos, switch, EndLevelSwitchFactory.createPhysics)
  }


  override def update(tpf: Float) {
    super.update(tpf) // throttled implementation
  }

  override def throttledUpdate(tpf: Float) {
    val answer = actorSystem.actorSelection("/user/level-" + level) ? AskLevelState
    answer.foreach({
      case LevelStateResponse(_,complete) => if (complete) advanceLevel()
    })
  }

  def advanceLevel() {
    stateManager.detach(this)
    stateManager.attach(next)
  }

  override def cleanup() {
    super.cleanup()
    actorSystem.actorSelection("/user/enemy-*") ! PoisonPill
    actorSystem.actorSelection("/user/ammo-*") ! PoisonPill
    actorSystem.actorSelection("/user/health-*") ! PoisonPill
    actorSystem.actorSelection("/user/level-*") ! PoisonPill
    actorSystem.actorSelection("/user/end-level-switch-*") ! PoisonPill
  }

  def next = new InitializeGameState(level + 1)
}