package com.stovokor.game.state

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.stovokor.actor.AskPosition
import com.stovokor.actor.PositionResponse
import com.stovokor.state.ActorSystemState
import com.stovokor.state.BaseState

import akka.pattern._
import akka.util.Timeout

class FPSActorCameraState extends BaseState with ThrottledUpdate {

  val maxAge = 1f / 60f // seconds

  implicit val timeout = Timeout(1.seconds)

  override def initialize(stateManager: AppStateManager, simpleApp: Application) {
    super.initialize(stateManager, simpleApp)
  }

  override def update(tpf: Float) {
    super.update(tpf) // throttled implementation
  }

  override def throttledUpdate(tpf: Float) {
    (actorSystem.actorSelection("/user/player") ? AskPosition).foreach({
      case PositionResponse(pos, _) => enqueueMain(() => {
        cam.setLocation(pos)
        // Don't set the rotation here.
        cam.update
      })
    })
  }
}
