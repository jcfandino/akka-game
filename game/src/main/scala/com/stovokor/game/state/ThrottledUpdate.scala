package com.stovokor.game.state

trait ThrottledUpdate {

  var age = 0f

  def update(tpf: Float) {
    age += tpf
    if (age > maxAge) {
      age = 0f
      throttledUpdate(age)
    }
  }

  def maxAge: Float
  def throttledUpdate(tpf: Float)
}