package com.stovokor.game.state

import com.jme3.input.controls.ActionListener
import com.jme3.app.state.AppStateManager
import com.jme3.math.Vector3f
import com.stovokor.game.actor.PlayerMove
import com.jme3.input.controls.AnalogListener
import com.jme3.app.Application
import com.jme3.input.InputManager
import com.stovokor.game.actor.Shoot
import com.jme3.input.KeyInput
import com.jme3.input.controls.KeyTrigger
import com.jme3.input.controls.MouseAxisTrigger
import com.jme3.input.MouseInput
import com.jme3.math.FastMath
import com.jme3.math.Quaternion
import com.stovokor.game.actor.PlayerRotate
import com.stovokor.game.actor.PlayerRotate
import com.stovokor.state.BaseState
import com.stovokor.state.ActorSystemState
import com.stovokor.actor.TellPosition

/**
 * State to read input to control the player and update the game logic with the desired movement.
 * Also updates the camera right away to avoid async delay and be responsive.
 */
class PlayerMovementState extends BaseState
    with ActionListener
    with AnalogListener {

  val rotFactorH = 1000f
  val rotFactorV = 400f

  var (up, down, left, right, shoot) = (false, false, false, false, false)
  var (rotDeltaH, rotDeltaV) = (FastMath.TWO_PI, 0f) // start with 2pi to force an update

  var rotationH = 0f // 0 ~ 2pi
  var rotationV = FastMath.HALF_PI // 0 ~ pi

  override def initialize(stateManager: AppStateManager, simpleApp: Application) {
    super.initialize(stateManager, simpleApp)
    setupKeys(inputManager)
  }

  def system = app.getStateManager.getState(classOf[ActorSystemState]).system
  def player = system.actorSelection("/user/player") //.resolveOne(1.second)

  var lastDir: Vector3f = Vector3f.UNIT_X

  override def update(tpf: Float) {
    updateDirection(tpf)
    updateRotation(tpf)

    if (shoot) {
      shoot = false
      player ! Shoot
    }
  }

  def updateDirection(tpf: Float) {
    val direction = new Vector3f()
    if (left) direction.addLocal(0, 0, 1)
    if (right) direction.addLocal(0, 0, -1)
    if (up) direction.addLocal(1, 0, 0)
    if (down) direction.addLocal(-1, 0, 0)
    direction.normalizeLocal

    if (direction.length() != 0f || lastDir.length() != 0f) {
      player ! PlayerMove(direction)
      lastDir = direction
    }
  }
  // TODO - cleanup
  def updateRotation(tpf: Float) {

    def rotateH(delta: Float) {
      val value = rotationH + rotFactorH * delta
      rotationH =
        if (value > FastMath.TWO_PI) value - FastMath.TWO_PI
        else if (value < 0f) value + FastMath.TWO_PI
        else value
    }

    def rotateV(delta: Float) {
      val value = rotationV + rotFactorV * delta
      rotationV =
        if (value > FastMath.PI) FastMath.PI
        else if (value < 0f) 0f
        else value
    }
    if (rotDeltaH != 0f || rotDeltaV != 0f) {
      rotateH(rotDeltaH)
      rotateV(rotDeltaV)
      rotDeltaH = 0f
      rotDeltaV = 0f
      // only update camera if fps camera is active
      if (stateManager.getState(classOf[FPSActorCameraState]) != null) {
        // front points to x (or should be z?)
        cam.setRotation(new Quaternion().fromAngles(rotationV - FastMath.HALF_PI, rotationH, 0f))
      }
      // maybe too many messages?
      player ! PlayerRotate(rotationH, rotationV)
    }
  }

  def reset(pos:Vector3f) {
    enqueueMain(() => {
      rotationH = 0
      rotationV = FastMath.HALF_PI
      rotDeltaH = 0
      rotDeltaV = 0
      if (stateManager.getState(classOf[FPSActorCameraState]) != null) {
        cam.setRotation(new Quaternion().fromAngles(rotationV - FastMath.HALF_PI, rotationH, 0f))
        cam.setLocation(pos)
      }
    })
  }

  def setupKeys(inputManager: InputManager) {
    // Keys
    inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A), new KeyTrigger(KeyInput.KEY_LEFT))
    inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D), new KeyTrigger(KeyInput.KEY_RIGHT))
    inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W), new KeyTrigger(KeyInput.KEY_UP))
    inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S), new KeyTrigger(KeyInput.KEY_DOWN))
    inputManager.addMapping("Space", new KeyTrigger(KeyInput.KEY_SPACE), new KeyTrigger(KeyInput.KEY_H))
    inputManager.addMapping("Reset", new KeyTrigger(KeyInput.KEY_R), new KeyTrigger(KeyInput.KEY_RETURN))
    inputManager.addListener(this, "Left")
    inputManager.addListener(this, "Right")
    inputManager.addListener(this, "Up")
    inputManager.addListener(this, "Down")
    inputManager.addListener(this, "Space")
    inputManager.addListener(this, "Reset")

    // Mouse
    inputManager.addMapping("TurnLeft", new MouseAxisTrigger(MouseInput.AXIS_X, true))
    inputManager.addMapping("TurnRight", new MouseAxisTrigger(MouseInput.AXIS_X, false))
    inputManager.addMapping("LookUp", new MouseAxisTrigger(MouseInput.AXIS_Y, true))
    inputManager.addMapping("LookDown", new MouseAxisTrigger(MouseInput.AXIS_Y, false))
    inputManager.addListener(this, "TurnLeft")
    inputManager.addListener(this, "TurnRight")
    inputManager.addListener(this, "LookUp")
    inputManager.addListener(this, "LookDown")
  }

  def onAction(binding: String, value: Boolean, tpf: Float) {
    (binding, value) match {
      case ("Left", _)  => left = value
      case ("Right", _) => right = value
      case ("Up", _)    => up = value
      case ("Down", _)  => down = value
      case ("Space", _) => shoot = value
      case _            =>
    }
  }

  def onAnalog(binding: String, value: Float, tpf: Float) {
    (binding) match {
      case ("TurnLeft")  => rotDeltaH += tpf * value
      case ("TurnRight") => rotDeltaH -= tpf * value
      case ("LookUp")    => rotDeltaV += tpf * value
      case ("LookDown")  => rotDeltaV -= tpf * value
      case _             =>
    }
  }

}