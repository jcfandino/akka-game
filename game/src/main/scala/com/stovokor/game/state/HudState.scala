package com.stovokor.game.state

import com.stovokor.state.BaseState
import com.jme3.font.BitmapText
import com.jme3.math.ColorRGBA
import com.jme3.app.state.AppStateManager
import com.jme3.app.Application
import com.stovokor.state.ActorSystemState
import com.stovokor.actor.AskPosition

import akka.util.Timeout
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import akka.pattern._
import com.stovokor.game.actor.AskPlayerState
import com.stovokor.game.actor.PlayerStateResponse
import com.jme3.font.BitmapFont
import com.stovokor.game.actor.AskLevelState
import com.stovokor.game.actor.LevelStateResponse

class HudState extends BaseState with ThrottledUpdate {

  val maxAge = .05f

  var playerStatusText: BitmapText = null
  var levelStatusText: BitmapText = null

  implicit val timeout = Timeout(50.millis)

  override def initialize(stateManager: AppStateManager, simpleApp: Application) {
    super.initialize(stateManager, simpleApp)
    implicit val font = assetManager.loadFont("Interface/Fonts/Default.fnt")
    playerStatusText = createTextField(250)
    levelStatusText = createTextField(500)
    guiNode.attachChild(playerStatusText)
    guiNode.attachChild(levelStatusText)
  }

  def createTextField(xcoor: Int)(implicit font: BitmapFont) = {
    val text = new BitmapText(font, false)
    text.setSize(font.getCharSet().getRenderedSize())
    text.setColor(ColorRGBA.White)
    text.setText(" -- loading --")
    text.setLocalTranslation(xcoor, text.getLineHeight(), 0)
    text
  }

  override def update(tpf: Float) {
    super.update(tpf) // throttled implementation
  }

  override def throttledUpdate(tpf: Float) {
    updatePlayerStatus()
    updateLevelStatus()
  }

  def updatePlayerStatus() {
    val answer = actorSystem.actorSelection("/user/player") ? AskPlayerState
    answer.foreach({
      case PlayerStateResponse(ammo, health) => {
        enqueueMain(() => playerStatusText.setText(s"Ammo: $ammo - Health: $health"))
      }
    })
  }

  def updateLevelStatus() {
    val answer = actorSystem.actorSelection("/user/level-*") ? AskLevelState
    answer.foreach({
      case LevelStateResponse(level, _) => {
        enqueueMain(() => levelStatusText.setText(s"Level: $level"))
      }
    })
  }
}