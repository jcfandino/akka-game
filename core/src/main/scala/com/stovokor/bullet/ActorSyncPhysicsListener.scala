package com.stovokor.bullet

import com.jme3.bullet.PhysicsTickListener
import com.jme3.bullet.PhysicsSpace
import akka.actor.ActorSystem
import akka.actor.ActorRef
import akka.pattern._
import akka.util.Timeout
import com.jme3.bullet.objects.PhysicsRigidBody
import com.stovokor.actor.LocationUpdate
import com.stovokor.actor.AskPosition
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import com.jme3.math.Vector3f
import com.stovokor.actor.PositionResponse
import com.stovokor.actor.AskMovement
import com.stovokor.actor.MovementResponse
import com.stovokor.actor.TellPosition
import com.jme3.math.Quaternion
import com.jme3.math.FastMath
import com.stovokor.actor.RelativeMovement
import com.stovokor.actor.AbsoluteMovement

/**
 * Listener to keep the physics simulation synced with the actor's game logic.
 */
class ActorSyncPhysicsListener(actor: ActorRef, body: PhysicsRigidBody) extends PhysicsTickListener {

  implicit val timeout = Timeout(1.seconds)

  def prePhysicsTick(space: PhysicsSpace, tpf: Float) = {
    // TODO: I could have two listeners: one for kinematic and not kinematic.
    // if kinematic it decides where it should be
    if (body.isKinematic) {
      val answer = actor ? AskPosition
      answer.foreach(pos => {
        val p = pos.asInstanceOf[PositionResponse]
        body.setPhysicsLocation(p.pos)
      })
    } else {
      // else we ask where does it want to go and update the simulation
      val answer = actor ? AskMovement
      answer.foreach({
        case MovementResponse(RelativeMovement(dir, _)) => {
          body.setLinearVelocity(new Vector3f(dir.x, body.getLinearVelocity.y, dir.z))
        }
        case MovementResponse(AbsoluteMovement(pos,rot)) => {
          body.setPhysicsLocation(pos)
          body.setPhysicsRotation(rot)
          body.setLinearVelocity(Vector3f.ZERO)
          body.clearForces()
        }
      })
    }
  }

  def physicsTick(space: PhysicsSpace, tpf: Float) = {
    if (!body.isKinematic) {
      // after the simulation we set where it was moved
      // println(s"TellPosition ${body.getPhysicsLocation}")
      actor ! TellPosition(body.getPhysicsLocation)
    }
  }

}