package com.stovokor.actor

import akka.actor.Props
import com.jme3.math.Vector3f
import com.jme3.math.Quaternion
import akka.actor.ActorRef
import com.jme3.bullet.collision.PhysicsCollisionEvent

case class TellPosition(pos: Vector3f)
case object AskPosition
case class PositionResponse(pos: Vector3f, rotation: Quaternion = Quaternion.ZERO)
case object AskMovement
case class MovementResponse(movement: Movement)
case class Tick(t: Double)
case class LocationUpdate(pos: Vector3f)
case class CollideWith(other: ActorRef, event: PhysicsCollisionEvent)

// movement types
abstract class Movement(rotation: Quaternion)
case class AbsoluteMovement(newPos: Vector3f, rotation: Quaternion) extends Movement(rotation)
case class RelativeMovement(dir: Vector3f, rotation: Quaternion) extends Movement(rotation)
