package com.stovokor.actor

import akka.actor.Actor
import akka.actor.ActorRef
import com.jme3.asset.AssetManager
import com.jme3.scene.Spatial
import com.jme3.scene.Node
import com.stovokor.state.ActorSystemState
import com.stovokor.control.SpatialActorControl
import java.util.concurrent.Callable
import com.jme3.math.Vector3f
import com.jme3.bullet.objects.PhysicsRigidBody
import com.jme3.bullet.BulletAppState
import com.stovokor.bullet.ActorSyncPhysicsListener
import akka.actor.Terminated

case class PhysicsActorSpawnRequest(pos: Vector3f, actor: ActorRef, factory: () => PhysicsRigidBody)

class PhysicsActorSpawner(systemState: ActorSystemState) extends Actor {

  var spawned: Map[ActorRef, PhysicsRigidBody] = Map()

  def receive = {
    case PhysicsActorSpawnRequest(pos, actor, factory) => {
      val body = factory()
      //TODO ask the actor for the pos
      systemState.enqueueMain(() => {
        val space = physicsSpace
        body.setUserObject(actor)
        body.setPhysicsLocation(pos)
        space.add(body)
        space.addTickListener(new ActorSyncPhysicsListener(actor, body))
      })
      spawned = spawned.updated(actor, body)
      context.watch(actor)
    }
    case Terminated(actor) => {
      spawned.get(actor)
        .foreach(body => systemState.enqueueMain(() => physicsSpace.remove(body)))
      spawned -= actor
    }
  }

  def physicsState = systemState.stateManager.getState(classOf[BulletAppState])
  def physicsSpace = physicsState.getPhysicsSpace
}