package com.stovokor.state

import java.util.concurrent.Callable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationDouble
import scala.concurrent.duration.DurationInt

import com.jme3.app.Application
import com.jme3.app.state.AppStateManager
import com.jme3.bullet.BulletAppState
import com.jme3.bullet.PhysicsSpace
import com.stovokor.actor.PhysicsActorSpawner
import com.stovokor.actor.SpatialActorSpawner
import com.stovokor.actor.Tick
import com.stovokor.bullet.ActorCollisionListener

import akka.actor.ActorSystem
import akka.actor.Props

class ActorSystemState extends BaseState {

  val system = ActorSystem("Game")
  val tickPeriod = (1.0 / 60)

  override def initialize(stateManager: AppStateManager, simpleApp: Application) {
    super.initialize(stateManager, simpleApp)

    // create spawner actors
    system.actorOf(Props(new SpatialActorSpawner(this)), spatialSpawnerName)

    // start tick
    system.scheduler.schedule(0.seconds, tickPeriod.seconds, () => {
      system.actorSelection("/user/*") ! Tick(tickPeriod)
    })

    // add actor collision listener
    val physicsState = stateManager.getState(classOf[BulletAppState])
    if (physicsState != null) {
      system.actorOf(Props(new PhysicsActorSpawner(this)), physicsSpawnerName)
      physicsState.getPhysicsSpace.addCollisionListener(new ActorCollisionListener)
    } else {
      println(s"Actor system running without physics")
    }
  }

  override def update(tpf: Float) {
  }

  override def cleanup() {
    system.terminate()
  }

}